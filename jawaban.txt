Soal 1 Membuat Database
create database myshop;

Soal 2 Membuat Table di Dalam Database
Table users
create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table categories
create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

Table items
create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(20),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );

Soal 3 Memasukkan Data pada Table
Table users
insert into users(name) values("John Doe"),("Jane Doe");
insert into users(email) values("john@doe.com"),("jane@doe.com");
insert into users(password) values("john123"),("jenita123");